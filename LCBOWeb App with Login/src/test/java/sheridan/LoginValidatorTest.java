package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Valid login" , LoginValidator.isValidLoginName( "abc1234" ) );
	}
	
	@Test
	public void testIsValidLoginException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "" ) );
	}

	@Test
	public void testIsValidLoginBoundaryIn( ) {
		assertTrue("Valid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Smith" ) );
	}
	
	@Test
	public void testIsValidLoginNumRegular( ) {
		assertTrue("Valid login" , LoginValidator.isValidLoginName( "Wilson123" ) );
	}
	
	@Test
	public void testIsValidLoginNumException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "=" ) );
	}

	@Test
	public void testIsValidLoginNumBoundaryIn( ) {
		assertTrue("Valid login" , LoginValidator.isValidLoginName( "Qin123" ) );
	}
	
	@Test
	public void testIsValidLoginNumBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "22222" ) );
	}
}
